package collections.comparing.generator;

import java.util.Objects;

public class CountryCapital {

    private String country;
    private String capital;

    public CountryCapital(String country, String capital) {
        this.country = country;
        this.capital = capital;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CountryCapital that = (CountryCapital) o;
        return Objects.equals(country, that.country) &&
                Objects.equals(capital, that.capital);
    }

    @Override
    public int hashCode() {
        return Objects.hash(country, capital);
    }
}
