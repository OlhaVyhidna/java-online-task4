package game.entity;

public class Player {

    private int points = 25;

    public void addPoints(int points){
        this.points+=points;
    }

    public int getPoints() {
        return this.points;
    }

    @Override
    public String toString() {
        return "Player{" +
                "points=" + points +
                '}';
    }
}
