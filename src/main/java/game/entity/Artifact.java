package game.entity;

import java.util.Random;

public class Artifact extends GameElement {

    private int power;

    public Artifact() {
        this.power = initPower();
    }

    private int initPower(){
        Random random = new Random();
        int max = 80;
        int min = 10;
        return (random.nextInt((max-min)+1)+min);
    }

    @Override
    public int getPower() {
        return this.power;
    }

    @Override
    public String toString() {
        return "Artifact " +
                "power=" + power;
    }
}
