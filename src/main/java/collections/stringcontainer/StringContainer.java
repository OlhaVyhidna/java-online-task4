package collections.stringcontainer;

public class StringContainer {

    private String [] strings;

    public StringContainer() {
        this.strings = new String[10];
    }

    public String getsStrings(int index) {
        return strings[index];
    }

    public void addsStrings(String s) {
        boolean isBigEnough = false;
        for (int i = 0; i < this.strings.length; i++) {
           if (this.strings[i]==null){
               strings[i]=s;
               isBigEnough = true;
               break;
           }
        }
        if (!isBigEnough){
          String []  newStrings = new String[(int) (this.strings.length * 1.5)];
            for (int i = 0; i < strings.length; i++) {
                newStrings[i] = this.strings[i];
            }
            newStrings[this.strings.length] = s;
            this.strings = newStrings;
        }
    }

    public String[] getStrings() {
        return strings;
    }

    public void setStrings(String[] strings) {
        this.strings = strings;
    }
}
