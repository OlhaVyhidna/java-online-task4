package game.entity;

import java.util.*;
import java.util.stream.Collectors;

public class Game {

    private Player player = new Player();
    private TreeMap<Integer, GameElement> doors = new TreeMap<>();

    public Game() {
        initDoors();
    }

    public Map<Integer, GameElement>  extractDoorsWithMonsters(){

        Map<Integer, GameElement> monster = this.doors.entrySet().stream().filter(door -> door.getValue().getClass()
                .getSimpleName().equals("Monster")).collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue()));

        return monster;
    }

    public Map<Integer, GameElement>  extractDoorsWithArtifacts(){

        Map<Integer, GameElement> monster = this.doors.entrySet().stream().filter(door -> door.getValue().getClass()
                .getSimpleName().equals("Artifact")).collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue()));

        return monster;
    }

    public  Map<Integer, GameElement> sortMapByValue(Map<Integer, GameElement> mapToSort){

        Map<Integer, GameElement> collect = mapToSort.entrySet().stream()
                .sorted(Map.Entry.comparingByValue())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

       return collect;
    }

    public Map<Integer, GameElement> sortMapByValueReverseOrder(Map<Integer, GameElement> mapToSort){

        Map<Integer, GameElement> collect = mapToSort.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

        return collect;
    }

    public ArrayList<Integer> findTheTrail(){
        Map<Integer, GameElement> allDoors = this.getDoors();
        ArrayList<Integer> doorsToVisit = new ArrayList<>(allDoors.keySet());
        ArrayList<Integer> trail = new ArrayList<>();
        Player player = this.getPlayer();
        int numberOfIteration = 0;

        while (doorsToVisit.size()>0) {
            Iterator<Integer> iterator = doorsToVisit.iterator();

            while (iterator.hasNext()) {

                Integer next = iterator.next();
                GameElement currentElement = null;

                currentElement = allDoors.get(next);
                String currentElementClass = currentElement.getClass().getSimpleName();

                if (currentElementClass.equals("Artifact")) {
                    player.addPoints(currentElement.getPower());
                    trail.add(next);
                    iterator.remove();

                } else if (currentElementClass.equals("Monster")) {

                    if (currentElement.getPower() <= player.getPoints()) {
                        trail.add(next);
                        iterator.remove();
                    }
                }
            }
            numberOfIteration++;
            if (numberOfIteration>allDoors.size()){
                System.out.println("This game can't be won");
                return null;
            }
        }

        return trail;
    }


    public int getCurrentNumberOfDeathDoors(int playerPoints){
        ArrayList<GameElement> monstersList = new ArrayList<>(this.extractDoorsWithMonsters().values());
        int sum = 0;
        int index = 0;

        return countNumberOfDeathDoors(index, sum, playerPoints, monstersList);
    }

    private int countNumberOfDeathDoors(int index, int sum, int playerPoints, ArrayList<GameElement> list){
        GameElement currentValue = list.get(index);
        if ((currentValue.getPower())>playerPoints){
            sum++;
        }
        index++;
        if (index<list.size()){
            sum = countNumberOfDeathDoors(index, sum, playerPoints, list);
        }
        return sum;
    }



    private void initDoors(){
        for (int i = 1; i < 11; i++) {
            this.doors.put(i, generateGameElement());

        }
    }

    private GameElement generateGameElement(){
        int elementType = (int) Math.round(Math.random());
        if (elementType==0){
            return new Monster();
        }else {
            return new Artifact();
        }

    }

    public TreeMap<Integer, GameElement> getDoors() {
        return doors;
    }

    public Player getPlayer() {
        return this.player;
    }
}
