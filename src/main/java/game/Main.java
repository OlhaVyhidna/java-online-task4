package game;

import game.entity.GameElement;

import java.util.TreeMap;

public class Main {

    public static void main(String[] args) {
        Menu menu = new Menu();
        TreeMap<Integer, GameElement> doors = menu.getGame().getDoors();
        doors.entrySet().forEach(door -> System.out.println(door.getKey() + " " + door.getValue()));
        System.out.println();
        menu.startupMenu();

    }
}
