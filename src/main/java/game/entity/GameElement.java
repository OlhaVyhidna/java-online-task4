package game.entity;

public abstract class GameElement implements Comparable<GameElement>{


    @Override
    public int compareTo(GameElement o) {
        return this.getPower()-o.getPower();
    }

   public abstract int getPower();


}
