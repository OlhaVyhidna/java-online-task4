package logical;

import logical.arrays.ArraysFirstTask;
import logical.arrays.ArraysSecondTask;
import logical.arrays.ArraysThirdTask;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        int[] firstArray = {2, 3, 3, 3, 4, 7, 8, 8, 13, 9, 9, 9, 9, 7, 15, 5};
        int[] secondArray = {3, 5, 7, 4, 9};

        ArraysFirstTask arraysFirstTask = new ArraysFirstTask();
        ArraysSecondTask arraysSecondTask = new ArraysSecondTask();
        ArraysThirdTask arraysThirdTask = new ArraysThirdTask();

        int[] resultArrayA = arraysFirstTask.existInBothArrays(firstArray, secondArray);
        int[] resultArrayB = arraysFirstTask.existOnlyInOneArray(firstArray, secondArray);
        final int[] moreThenTwice = arraysSecondTask.deleteNumbersThatRepeatedMoreThenTwice(firstArray);
        int[] ints = arraysThirdTask.deleteDuplicates(firstArray);


        System.out.println(Arrays.toString(resultArrayA));
        System.out.println();
        System.out.println(Arrays.toString(resultArrayB));
        System.out.println();
        System.out.println(Arrays.toString(moreThenTwice));
        System.out.println();
        System.out.println(Arrays.toString(ints));
    }


}
