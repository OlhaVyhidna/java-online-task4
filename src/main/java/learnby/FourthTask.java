package learnby;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class FourthTask {
    private File file;

    public FourthTask(String filePath) {
        this.file = new File(filePath);
    }

    public ArrayList<String> sortCollectionByLineLength() throws IOException {
        ArrayList<String> poemInCollection = writePoemInCollection();
        poemInCollection.sort((o1, o2) -> o1.length() - o2.length());
        return poemInCollection;
    }

    public ArrayList<String> writePoemInCollection() throws IOException {
        ArrayList<String> list = new ArrayList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(this.file))) {
            String line = bufferedReader.readLine();
            while (line != null) {
                list.add(line);
                line = bufferedReader.readLine();
            }
        }
        return list;
    }
}
