package logical.arrays;

import java.util.Arrays;

public class ArraysThirdTask {

    public int [] deleteDuplicates(int[] array){
        Integer[] newArray = new Integer[array.length];
        Arrays.sort(array);
        int newArrayIndex = 1;
        int current = 0;
        int next = 0;
        newArray[0]=array[0];
        for (int i = 1; i < array.length; i++) {
            current = i-1;
            next = i;

            if (array[current]!=array[next]){
                newArray[newArrayIndex] = array[next];
                newArrayIndex++;
            }
        }
        return trimArray(newArray);
    }

    static int[] trimArray(Integer[] array){
        int length = arrayLength(array);
        int[] currentArray = new int[length];

        for (int i = 0; i < length; i++) {
            currentArray[i]=array[i];
        }
        return currentArray;
    }

    private static int arrayLength(Integer[] array){
        int length = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i]!=null){
                length++;
            }
        }
        return length;
    }
}

