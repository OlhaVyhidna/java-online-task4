package game.entity;

import java.util.Random;

public class Monster extends GameElement{

    private int power;

    public Monster() {
        this.power = initPower();
    }

    private int initPower(){
        Random random = new Random();
        int max = 100;
        int min = 5;
        return (random.nextInt((max-min)+1)+min);
    }

    public int getPower() {
        return power;
    }

    @Override
    public String toString() {
        return "Monster " +
                "power=" + power;
    }
}
