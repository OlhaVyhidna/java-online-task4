package learnby;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Stack;

public class Main {

    public static void main(String[] args) throws IOException {

        FirstTask firstTask = new FirstTask("firstTask");
        SecondTask secondTask = new SecondTask();
        FourthTask fourthTask = new FourthTask("fourthTask");

        Stack<String> stack;
        //first task
        stack = firstTask.readFromFile();
        firstTask.writeInFile(stack);
        //second task
        Integer integer = secondTask.getNumberReverse(234);
        System.out.println(integer);
        //fourth task
        System.out.println(fourthTask.sortCollectionByLineLength());

        //Fifth task
        ArrayList<String> stringArrayList = fourthTask.writePoemInCollection();
        Collections.sort(stringArrayList);
        System.out.println(stringArrayList);

    }
}
