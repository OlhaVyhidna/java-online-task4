package collections.comparing.generator;

import collections.comparing.generator.CountryCapital;

import java.util.ArrayList;

public class CustomGenerator {

    ArrayList<CountryCapital> countryCapital = new ArrayList<>();

    public CustomGenerator() {
        fillTheList();
    }

    private void fillTheList(){
        this.countryCapital.add(new CountryCapital("Australia", "Canberra"));
        this.countryCapital.add(new CountryCapital("Austria", "Vienna"));
        this.countryCapital.add(new CountryCapital("Belarus", "Minsk"));
        this.countryCapital.add(new CountryCapital("Brazil", "Brasilia"));
        this.countryCapital.add(new CountryCapital("China", "Beijing"));
        this.countryCapital.add(new CountryCapital("Denmark", "Copenhagen"));
        this.countryCapital.add(new CountryCapital("Egypt", "Cairo"));
        this.countryCapital.add(new CountryCapital("France", "Paris"));
        this.countryCapital.add(new CountryCapital("Guatemala", "Guatemala City"));
        this.countryCapital.add(new CountryCapital("Honduras", "Tegucigalpa"));
        this.countryCapital.add(new CountryCapital("Ireland", "Dublin"));
        this.countryCapital.add(new CountryCapital("Jamaica", "Kingston"));
        this.countryCapital.add(new CountryCapital("Libya", "Tripoli"));
        this.countryCapital.add(new CountryCapital("Morocco", "Rabat"));
        this.countryCapital.add(new CountryCapital("Nicaragua", "Managua"));
        this.countryCapital.add(new CountryCapital("Oman", "Muscat"));
        this.countryCapital.add(new CountryCapital("Paraguay", "Asunción"));
        this.countryCapital.add(new CountryCapital("Qatar", "Doha"));
        this.countryCapital.add(new CountryCapital("Rwanda", "Kigali"));
        this.countryCapital.add(new CountryCapital("Somalia", "Mogadishu"));
        this.countryCapital.add(new CountryCapital("Togo", "Lomé"));
        this.countryCapital.add(new CountryCapital("Uganda", "Kampala"));
        this.countryCapital.add(new CountryCapital("Vanuatu", "Port Vila"));
        this.countryCapital.add(new CountryCapital("Vietnam", "Hanoi"));
        this.countryCapital.add(new CountryCapital("Yemen", "Sana'a"));
        this.countryCapital.add(new CountryCapital("Zambia", "Lusaka"));

    }

    public CountryCapital getCountryAndCapitalByIndex(int index){
        return this.countryCapital.get(index);
    }

    public ArrayList<CountryCapital> getCountryCapital() {
        return countryCapital;
    }

    public void setCountryCapital(ArrayList<CountryCapital> countryCapital) {
        this.countryCapital = countryCapital;
    }
}
