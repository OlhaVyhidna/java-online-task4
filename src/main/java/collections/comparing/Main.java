package collections.comparing;

import collections.comparing.generator.CountryCapital;
import collections.comparing.generator.CustomGenerator;
import collections.comparing.generator.MyCustomBinarySearch;

import java.util.*;

public class Main {


    public static void main(String[] args) {

        CustomGenerator customGenerator = new CustomGenerator();
        int randomMax = customGenerator.getCountryCapital().size();
        int arraysSize = 15;
        Random random = new Random();
        MyCustomBinarySearch myBinarySearch = new MyCustomBinarySearch();

        ArrayList<ObjectToCompare> list = new ArrayList<>();
        ObjectToCompare[] array = new ObjectToCompare[arraysSize];

        //create a validation object
        ObjectToCompare validationObject = new ObjectToCompare("Ukraine", "Kyiv");

        //add it as a first element
        list.add(validationObject);
        array[0] = validationObject;

        //fill containers by random objects
        for (int i = 1; i < arraysSize; i++) {
            CountryCapital countryAndCapitalByIndex = customGenerator
                    .getCountryAndCapitalByIndex(random.nextInt(randomMax));

            ObjectToCompare objectToCompare = new ObjectToCompare(countryAndCapitalByIndex.getCountry(),
                    countryAndCapitalByIndex.getCapital());

            list.add(objectToCompare);
            array[i]=objectToCompare;

        }


        printResults(list, array);

        //sort by first field
        Collections.sort(list);
        Arrays.sort(array);
        System.out.println("\n");

        printResults(list, array);

        //sort by second field
        list.sort(new CompareBySurname());
        Arrays.sort(array, new CompareBySurname());
        System.out.println("\n");

        printResults(list, array);

        //use binary search
        System.out.println("\n");
        myBinarySearch.binarySearchInArray(validationObject, array);
        myBinarySearch.binarySearchInList(validationObject, list);


    }

    private static void printResults(ArrayList<ObjectToCompare> list, ObjectToCompare[] array){

        System.out.println(list);
        System.out.println();
        System.out.println(Arrays.toString(array));
    }
}
