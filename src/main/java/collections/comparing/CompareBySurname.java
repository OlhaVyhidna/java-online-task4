package collections.comparing;

import java.util.Comparator;

public class CompareBySurname implements Comparator<ObjectToCompare> {

    @Override
    public int compare(ObjectToCompare o1, ObjectToCompare o2) {
        String surnameO1 = o1.getSurname();
        String surnameO2 = o2.getSurname();
        return surnameO1.compareTo(surnameO2);
    }

}
