package logical.arrays;

public class ArraysSecondTask {

    public int [] deleteNumbersThatRepeatedMoreThenTwice(int[] array){

        Integer[] newArray = new Integer[array.length];
        int newArrayIndex = 0;
        for (int i = 0; i < array.length; i++) {
            int currentValue = array[i];
            if (!repeatedMoreThenTwice(currentValue, array)){
                newArray[newArrayIndex]=array[i];
                newArrayIndex++;
            }
        }
        return ArraysThirdTask.trimArray(newArray);
    }

    private boolean repeatedMoreThenTwice(int value, int[] array){
        int numberOfRepeats = 0;

        for (int i = 0; i < array.length; i++) {
            if (array[i]==value){
                numberOfRepeats++;
            }
        }

        if (numberOfRepeats>2){
            return true;
        }
        return false;
    }



}
