package collections.stringcontainer;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        long arrayStart = System.nanoTime();
        StringContainer stringContainer = new StringContainer();
        stringContainer.addsStrings("one");
        stringContainer.addsStrings("two");
        stringContainer.addsStrings("two");
        stringContainer.addsStrings("tree");
        stringContainer.addsStrings("tree");
        stringContainer.addsStrings("tree");
        stringContainer.addsStrings("four");
        stringContainer.addsStrings("four");
        stringContainer.addsStrings("four");
        stringContainer.addsStrings("four");
        stringContainer.addsStrings("five");
        stringContainer.addsStrings("five");
        stringContainer.addsStrings("five");
        stringContainer.addsStrings("five");
        stringContainer.addsStrings("five");
        stringContainer.addsStrings("six");
        stringContainer.addsStrings("six");
        stringContainer.addsStrings("six");
        stringContainer.addsStrings("six");

        long arrayEnd = System.nanoTime();
        System.out.println("Array execution - " + (arrayEnd-arrayStart));

        long listStart = System.nanoTime();
        ArrayList<String> comparingList = new ArrayList<>();
        comparingList.add("one");
        comparingList.add("two");
        comparingList.add("two");
        comparingList.add("tree");
        comparingList.add("tree");
        comparingList.add("four");
        comparingList.add("four");
        comparingList.add("four");
        comparingList.add("four");
        comparingList.add("five");
        comparingList.add("five");
        comparingList.add("five");
        comparingList.add("five");
        comparingList.add("five");
        comparingList.add("six");
        comparingList.add("six");
        comparingList.add("six");
        comparingList.add("six");

        long listEnd = System.nanoTime();

        System.out.println("List execution - " + (listEnd-listStart));

    }
}
