package collections.comparing.generator;

import collections.comparing.CompareBySurname;
import collections.comparing.ObjectToCompare;

import java.util.List;

public class MyCustomBinarySearch {

    public MyCustomBinarySearch() {
    }

    public void binarySearchInList(ObjectToCompare objectToCompare, List<ObjectToCompare> list) {
        int indexToSearchIn = (list.size() - 1) / 2;
        CompareBySurname compareBySurname = new CompareBySurname();
        ObjectToCompare currentObject = null;
        while (list.size() >= 2) {
            ObjectToCompare objectInSearchedIndex = list.get(indexToSearchIn);
            int resultOfComparing = compareBySurname.compare(objectToCompare, objectInSearchedIndex);
            if (resultOfComparing == 0) {
                currentObject = objectInSearchedIndex;
                break;
            } else if (resultOfComparing < 0) {
                list = list.subList(0, indexToSearchIn + 1);
            } else if (resultOfComparing > 0) {
                list = list.subList(indexToSearchIn + 1, list.size());
            }
            indexToSearchIn = indexToSearchIn / 2;
        }
        if (currentObject != null) {
            System.out.println("Object was found");
            System.out.println(currentObject);
        } else {
            System.out.println("object wasn't found");
        }
    }

    public void binarySearchInArray(ObjectToCompare objectToCompare, ObjectToCompare[] array) {
        int indexToSearchIn = (array.length - 1) / 2;
        CompareBySurname compareBySurname = new CompareBySurname();
        ObjectToCompare currentObject = null;
        while (array.length >= 2) {
            ObjectToCompare objectInSearchedIndex = array[indexToSearchIn];
            int resultOfComparing = compareBySurname.compare(objectToCompare, objectInSearchedIndex);
            if (resultOfComparing == 0) {
                currentObject = objectInSearchedIndex;
                break;
            } else if (resultOfComparing < 0) {
                array = subArray(0, indexToSearchIn + 1, array);
            } else if (resultOfComparing > 0) {
                array = subArray(indexToSearchIn + 1, array.length, array);
            }
            indexToSearchIn = indexToSearchIn / 2;
        }
        if (currentObject != null) {
            System.out.println("Object was found");
            System.out.println(currentObject);
        } else {
            System.out.println("object wasn't found");
        }
    }

    private ObjectToCompare[] subArray(int startIndex, int endIndex, ObjectToCompare[] objectToCompares) {
        ObjectToCompare[] currentArray = new ObjectToCompare[endIndex - startIndex];
        for (int i = 0; startIndex < endIndex; i++) {
            currentArray[i] = objectToCompares[startIndex];
            startIndex++;
        }
        return currentArray;
    }
}
