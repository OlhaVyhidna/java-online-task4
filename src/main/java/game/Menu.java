package game;

import game.entity.Game;
import game.entity.GameElement;
import game.entity.Player;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.TreeMap;

public class Menu {
    private Game game = new Game();
    private boolean lostGame;

    public void startupMenu(){
        System.out.println("Welcome in startup menu. Please choose an option: ");
        mainMenu();

    }

    private void mainMenu(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("For getting numbers of doors, opening which you will die, enter - 1");
        System.out.println("For getting trail of doors, follow which you will win, if it's possible, enter - 2");
        System.out.println("To open a door enter - 3");
        switch (scanner.nextInt()){
            case 1:{
                int numberOfDeathDoors = game.getCurrentNumberOfDeathDoors(game.getPlayer().getPoints());
                System.out.println("Behind " + numberOfDeathDoors + " of doors are monsters stronger then you.");
                break;
            }
            case 2:{
                ArrayList<Integer> theTrail = game.findTheTrail();
                System.out.println("The trail is: " + theTrail);
                break;
            }
            case 3:{
                openDoorMenu(scanner);
                break;
            }
            default:{
                System.out.println("You entered wrong number. Pleas try again");
                mainMenu();
                break;
            }
        }

        if (lostGame){
            return;
        }

        if (this.game.getDoors().size()==0){
            System.out.println("You opened all doors and stayed alive!");
            System.out.println("Congratulations, you won the game");
            return;
        }

        System.out.println("If you want to continue work enter 1, if you don't another number");
        int decision = scanner.nextInt();
        if (decision==1){
            mainMenu();
        }



        scanner.close();

    }

    private void openDoorMenu(Scanner scanner){
        TreeMap<Integer, GameElement> doors = this.game.getDoors();
        ArrayList<Integer> keysList= new ArrayList<>(doors.keySet());
        Player player = this.game.getPlayer();
        System.out.println("You didn't visited doors number: " + keysList);

        System.out.println("Enter number of door");
        Integer doorNumber = scanner.nextInt();
        if (!keysList.contains(doorNumber)){
            System.out.println("You entered wrong number. Pleas try again");
            openDoorMenu(scanner);
        }

        GameElement currentElement = doors.get(doorNumber);
        int currentElementPower = currentElement.getPower();
        String className = currentElement.getClass().getSimpleName();
        if (className.equals("Artifact")){
            System.out.println("Behind the door is an artifact, your power growth for " + currentElementPower + " points.");
            player.addPoints(currentElementPower);
            doors.remove(doorNumber);
        }else if (className.equals("Monster")){
            System.out.println("Behind the door is a monster.");
            if (currentElementPower<=player.getPoints()){
                System.out.println("You won in this fight!");
                doors.remove(doorNumber, currentElement);
            }else if (currentElementPower>player.getPoints()){
                System.out.println("Monster is stronger then you.");
                System.out.println("You lost the fight.");
                this.lostGame = true;
            }
        }

    }

    public Game getGame() {
        return this.game;
    }
}
