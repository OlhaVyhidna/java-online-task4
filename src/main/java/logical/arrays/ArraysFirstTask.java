package logical.arrays;

public class ArraysFirstTask {
    private  int valuesThatExistInBothArraysIndex;

    public int[] existInBothArrays(int[] firstArray, int[] secondArray) {

        this.valuesThatExistInBothArraysIndex = 0;

        int [] valuesThatExistInBothArrays = initArray(firstArray.length+secondArray.length);

       valuesThatExistInBothArrays = fillNewArrayIfExistInBoth(firstArray, valuesThatExistInBothArrays, secondArray);
       valuesThatExistInBothArrays = fillNewArrayIfExistInBoth(secondArray, valuesThatExistInBothArrays, firstArray);

       valuesThatExistInBothArrays = trimArray(valuesThatExistInBothArrays);

       return valuesThatExistInBothArrays;
    }

    public int[] existOnlyInOneArray(int[] firstArray, int[] secondArray){
        this.valuesThatExistInBothArraysIndex = 0;

        int [] valuesThatExistInBothArrays = initArray(firstArray.length+secondArray.length);

        valuesThatExistInBothArrays = fillNewArrayIfExistOnlyInOne(firstArray, valuesThatExistInBothArrays, secondArray);
        valuesThatExistInBothArrays = fillNewArrayIfExistOnlyInOne(secondArray, valuesThatExistInBothArrays, firstArray);

        valuesThatExistInBothArrays = trimArray(valuesThatExistInBothArrays);

        return valuesThatExistInBothArrays;
    }

    private int[] fillNewArrayIfExistInBoth(int[] checkingArray, int[] newArray, int[] existedArray){
        for (int i = 0; i < checkingArray.length; i++) {
            int currentValue = checkingArray[i];
            if (arrayContainsValue(currentValue, existedArray)&&
                    !arrayContainsValue(currentValue, newArray)){
                newArray[this.valuesThatExistInBothArraysIndex] = currentValue;
                this.valuesThatExistInBothArraysIndex++;
            }
        }

        return newArray;
    }

    private int[] fillNewArrayIfExistOnlyInOne(int[] checkingArray, int[] newArray, int[] existedArray){
        for (int i = 0; i < checkingArray.length; i++) {
            int currentValue = checkingArray[i];
            if (!arrayContainsValue(currentValue, existedArray)&&
                    !arrayContainsValue(currentValue, newArray)){
                newArray[this.valuesThatExistInBothArraysIndex] = currentValue;
                this.valuesThatExistInBothArraysIndex++;
            }
        }

        return newArray;
    }


    private boolean arrayContainsValue(int value, int [] array){
        for (int i = 0; i < array.length; i++) {
            if (array[i]==value){
                return true;
            }
        }
        return false;
    }

    private int[] trimArray(int[] array) {

        int length = getLength(array);
        int[] currentArray = new int[length];

        for (int j = 0; j <length ; j++) {
            currentArray[j] = array[j];
        }
        return currentArray;
    }

    private int getLength(int [] array){
        int length = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] != -1) {
                length++;
            }
        }
        return length;
    }

    private int[] initArray(int length) {
        int[] array = new int[length];
        for (int i = 0; i < array.length; i++) {
            array[i] = -1;
        }
        return array;
    }
}
