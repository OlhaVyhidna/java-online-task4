package learnby;

import java.util.Collections;
import java.util.Stack;

public class SecondTask {



    public Integer getNumberReverse(Integer inputNumber){
         Stack<Integer> stack = putNumberInStack(inputNumber);
         Integer reverseNumber = 0;
         while (!stack.empty()){
             reverseNumber = reverseNumber*10+stack.pop();
         }
         return reverseNumber;
    }

    private Stack<Integer> putNumberInStack(Integer inputNumber){
        Integer currentNumber = inputNumber;
        Stack<Integer> stack = new Stack<>();
        while (currentNumber > 0) {
            stack.push( currentNumber % 10 );
            currentNumber = currentNumber / 10;
        }
        Collections.reverse(stack);
        return stack;
    }
}
