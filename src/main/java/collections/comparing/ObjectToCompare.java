package collections.comparing;

import java.util.Objects;

public class ObjectToCompare implements Comparable<ObjectToCompare> {

    private String name;
    private String surname;

    public ObjectToCompare(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    @Override
    public int compareTo(ObjectToCompare o) {
        return this.getName().compareTo(o.getName());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSourname(String surname) {
        this.surname = surname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ObjectToCompare that = (ObjectToCompare) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(surname, that.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname);
    }

    @Override
    public String toString() {
        return "ObjectToCompare{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }
}
